import styledComponents from "styled-components";

const SignIn = styledComponents.button`
    padding: 15px 30px 15px 30px;
    cursor: pointer;
    background: #3A68B8;
    border-radius: 5px;
    border: none;
    font-family: 'Open Sans';
    font-weight: 600;
    font-size: 16px;
    line-height: 22px;
    color: #fff;
`

const HeaderButton = () => {


    return (
        <SignIn>
            Sign In
        </SignIn>
    )
}

export default HeaderButton;
import styledComponents from "styled-components";
import { Link } from "react-router-dom";

const Menu = styledComponents.ul`
    display: flex;
    justify-content: space-between;
    max-width: 411px;
    width: 100%;

    li > a {
        font-weight: 600;
        font-size: 16px;
        line-height: 22px;
        color: #FFFFFF;
        font-family: 'Open Sans', sans-serif;
    }
`

const HeaderMenu = (props) => {
    console.log(props);

    return (
        <Menu>
            {
                props.menu.map((item) => (
                    <li key={item.id}>
                        <Link to={item.href}>{item.name}</Link>
                    </li>
                ))
            }
        </Menu>
    )
}

export default HeaderMenu;
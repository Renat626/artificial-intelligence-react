import styledComponents from "styled-components";
import HeaderLogo from "./header__logo/HeaderLogo";
import HeaderMenu from "./header__menu/HeaderMenu";
import HeaderButton from "./header__button/HeaderButton";

const HeaderContainer = styledComponents.header`
    background-color: #0a121f;
    padding-top: 24px;
    padding-bottom: 24px;
`

const MenuContainer = styledComponents.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin: 0 auto;
    max-width: 1146px;
`

const Header = () => {
    const menu = [
        {id: 1, name: "Home", href: "#"},
        {id: 2, name: "About", href: "#"},
        {id: 3, name: "Services", href: "#"},
        {id: 4, name: "Blog", href: "#"},
        {id: 5, name: "Contact", href: "#"},
    ]

    return (
        <HeaderContainer>
            <MenuContainer>
                <HeaderLogo />
                <HeaderMenu menu={menu} />
                <HeaderButton />
            </MenuContainer>
        </HeaderContainer>
    )
}

export default Header;
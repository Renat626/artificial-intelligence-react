import Logo from "../../../assets/img/header__logo.png";
import { Link } from "react-router-dom";

const HeaderLogo = () => {


    return (
        <Link to="/">
            <img src={Logo} alt="logo" />
        </Link>
    )
}

export default HeaderLogo;